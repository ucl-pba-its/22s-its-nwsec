---
Week: 38
tags:
- Load balancing
- Networking
- Web server
---

\pagebreak

# Opgaver for ww38

Netværkstopologier

## Opgave 1 - Load balancing

Vi skal lave et netværk hvor vi bruger HA proxy eller relayd til at lave load balancing.

Dette er en Joint effort opgave, hvor vi laver den sammen på tavlen og på PC'erne.

### Opgave beskrivelse

0. Lave repo på gitlab
1. Beslutte en relevant topologi og tests
   Docs på gitlab
2. Sætte "worker" servere op
   Docs/configs på gitlab
3. Beslutte load balancer. HA Proxy eller relayd eller ?
   Docs på gitlab
4. Installere loadbalancer VM, OS+software
   Configs på gitlab
5. Teste
   Docs på gitlab

### Links

Noteark er [her](https://pad.tyk.nu/p/190920_loadbalancer)
