---
Week: 41
tags:
- Networking
- Web server
- Reverse proxy
- Goaccess
- Log
- Graylog
- Monitoring
---


\pagebreak

# Opgaver for ww41

Implementering af monitorering

## Opgave 1 - goaccess

I har allerede en webserver og en reverse proxy, som vi vil overvåge vha. [goaccess](https://goaccess.io/).

### Opgavebeskrivelse

1. Installer goaccess på både webserveren og på reverse proxy
2. Start `goaccess` real-time

    `goaccess /var/log/nginx/access.log -o /var/www/html/real-time.html  --log-format=COMBINED --real-time-html`

    Der er noget med fonte....

3. Generér lidt trafik på hjemmesiden, og se hvad der sker.

### Links

Ingen


## Opgave 2 - graylog

loggly virkede underligt efter at solarwinds havde overtaget det.

### Opgavebeskrivelse

1. Brug [Graylog appliance](https://docs.graylog.org/en/3.1/pages/installation/virtual_machine_appliances.html)

    Bruge VMwar workstation (ikke virtualbox)

2. Start VM'en

3. Sæt webserveren til at sende log data til graylog serveren. Se e.g. [graylog.org](https://marketplace.graylog.org/addons/a47beb3b-0bd9-4792-a56a-33b27b567856)

4. Brug wireshark til at se hvad der foregår. Brug evt. [`logger`](https://www.networkworld.com/article/3274570/using-logger-on-linux.html) programmet til at generere entries.

    Prøv evt. `watch "date | logger"`

### Links

Ingen


## Opgave 3 - Nagios eller librenms

Vi sætter Nagios eller librenms op sammen.


### Opgavebeskrivelse

TBA


### Links

Ingen
