---
Week: 44
tags:
- Networking
- Wireshark
- TCP dump
- Mirror port
- Openvswitch
- Juniper SRX
- Netflow
- Ntopng
---

\pagebreak

# Exercises for ww44

## Exercise 1a - Tcpdump and wireshark

Text mode vs. GUI

### Exercise instructions

1. In the Kali VM, generate some web trafic.
2. Find the trafic using `wireshark`
3. Using `tcpdump` on the command line, find the same trafic
4. Compare and evaluate

### Links

None supplied

## Exercise 1b - Tcpdump and pcap files

Tcpdump can save and read pcap files. Try it.

### Exercise instructions

1. In the Kali VM, generate some web trafic.
2. Save it using `tcpdump -w`
3. open the pcap file in wireshark and see the result.
4. Read the pcap using `tcpdump -r`


### Links

None supplied



## Exercise 2 - Sniffing as a router

When you use Kali in a VM, your host is acting as a router.

### Exercise instructions

1. In the Kali VM, generate some trafic, e.g web trafic.
2. Find the trafic using `wireshark` or `tcpdump`
3. On the host, sniff the trafic on the internal interface, ie. the interface Kali is connected to.
4. Compare and evaluate
3. On the host, sniff the trafic on the external interface, ie. the interface with the internet connection
4. Compare and evaluate - any difference from before?


### Links

None supplied




## Exercise 3a - Setting up a mirror port on JunOS

This required a managed switch like the EX4200 or some router with lots of ports like the SRX240.

### Exercise instructions

1.  Decide on a network with a switch.

    Virtual or physical at your discretion. This requires a device with at least 3 interfaces

2. decide on how to test

3. Implement and test

### Links

* Juniper reference for (mirror ports on SRX](https://www.juniper.net/documentation/en_US/junos/topics/task/configuration/configuring-port-mirroring-srx.html)


## Exercise 3b - Setting up a mirror port on linux using openvswitch

Openvswitch is a software switch for linux (and other unices).

### Exercise instructions

1.  Read this exercise and make a diagram showing what is asked of you.

2.  Set up a linux VM with at least three interface and two "internal" minimal VMs.

    The internal ones will be the one that does the sniffing, and the other will generate the trafic.

    The miror interface must not have an ip address.

3. Set up Openvswitch to switch between the two "noraml" interfaces

    The general documentation is [here](https://docs.openvswitch.org/en/latest/) which includes some  words on [installation](https://docs.openvswitch.org/en/latest/intro/install/distributions/#debian).

    An example of setting up [Openvswitch](https://devinpracticecom.wordpress.com/2016/10/18/open-vswitch-introduction-part-1/)

4. tests that it works

5. Set up a mirror port

    see .e.g the [official docs](http://docs.openvswitch.org/en/latest/faq/configuration/)

6. test and document

### Links

None

## Exercise 4 - Netflow

Netflow is basically summaries of connections.

### Exercise instructions

1. On the kali machine, install ntopng

    see e.g. [linode.com](https://www.linode.com/docs/networking/diagnostics/install-ntopng-for-network-monitoring-on-debian8/)

    Note that verification is done using `ethtool --show-offload eth0 | grep segmentation`

    Starting the redis server is done using `redis-server /etc/redis/redis.conf`

2. Generate some trafic and see the result in the web interface.

### Links

* An intro to netflow and ipfix from [flowmon](https://www.flowmon.com/en/solutions/use-case/netflow-ipfix)
