---
Week: 46
tags:
- Networking
- Firewall
- Design
---

\pagebreak

# Opgaver for ww46

## Exercise 1 - Subnets og firewall regler for projekt 2.

Vores setup i juniperlab har nogle services og den slags.

Har vi det rigtige design? hvilke regler og subnets vil være godt.

## Opgave beskrivelse

1. Find eller lav diagrammet, samt design og requirements for projekt 2.
2. Lav tabellen over hvilke subnets der må snakke med hvem, samt lave en liste over specifikke port+ip der er undtagelser.
3. Gruppér serverne efter deres netværksbehov.
4. Lav design, permissions table og specifikke regler for et nyt design hvor serverne er i flere grupper.
5. Formulér en plan for hvordan deet kan implementeres.

### Links

None supplied


## Exercise 2 - ww39 opgave reboot

Er vi blevet bedre til det her?

Det var opgaven hvor jeg brugte ublu lang tid på ikke at installere en APU2.

## Opgave beskrivelse

1. Find noterne frem fra uge 39.

2. Push noter til gitlab

    Vi skal oprette et repo.

3. Fælles status, og lave en todo liste.

    Denne inkluderer at lave et design/diagrammer/fw regler mv.

4. Udføre todo listen.

### Links

None supplied
