---
Week: 49
tags:
- Networking
- Passive DNS
- DNS logs
- Sniffing traffic
---

\pagebreak

# Opgaver for ww49

## Exercise 1 - Passive DNS

farsight giver API adgang til deres database.

### Opgave beskrivelse

1. Kildekritik: Hvem er [farsight security](https://www.farsightsecurity.com)?
2. [sign up](https://www.farsightsecurity.com/get-started-guide/) til en 30 dages licens

3. Brug enten API'en eller deres [scout program](https://www.farsightsecurity.com/tools/dnsdb-scout/)

3. Undersøg f.eks. ucl.dk for subdomains, mailservere og om der er noget andet at bemærke.

    Kig evt. på UCL ip block

4. PP på tavle, og plenum diskussion.

### Links

* [5 tips](https://www.farsightsecurity.com/txt-record/2017/10/18/bapril-threathuntingtips/)
* [The bailiwick thing](https://www.farsightsecurity.com/txt-record/2017/03/21/stsauver-what-is-a-bailiwick/)



## Exercise 2 - DNS log on windows server

I har en Windows server med et AD kørende. Dns har indbygget en log server.

### Opgave beskrivelse

1. Snak med Jacob og få vist de DNS queries der kommer.

2. Hvor meget fortæller det om Jeres brugere?


## Exercise 2 - Sniffing traffic (again)

Vi har lidt mirror porte på switche i juniper lab

### Opgave beskrivelse

1. Connect til port 3, og se at der er trafik.

    Port 1 og port 2 er hhv. outgoing og incoming traffic

    Den øverste blå cisco, og den nederste grå cisco switch (SEE-xx-xx) er på hver deres netværk.

2. Gem 10 min trafik, eller 100 MB.

    Vi ønsker en mængde som kan håndteres nemt.

3. Analyser trafikken. Find subnets, services, mv.

    Lav diagrammer, tabeller mv.

### Links

* L3 from pcaps [guide](https://eal-it-technology.github.io/itt-guides/guides/Building_L3_from_pcaps.html)



## Bonus exercise 1 - DNS visualization

Havde vi en ELKstack kørende kunne vi sætte noget [visualisering](https://www.politoinc.com/post/2018/02/05/how-to-build-your-own-dns-sinkhole-and-dns-logs-monitoring-system) op
