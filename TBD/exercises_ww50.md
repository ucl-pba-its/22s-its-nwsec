---
Week: 50
tags:
- Networking
- nmap
---

\pagebreak

# Opgaver for ww50

## Exercise 1 - ping test

en intro opgave til nmap

### Opgave beskrivelse

1. Hent ip listen fra ucl red team projektet. Under noter/domain-search/ping-sweep

2. Lav et script i bash som ping-checker alle ip adresser i en given fil.

    Brug en test fil med meget få entries til udviklingen.

3. Kør den på hele listen

4. Findes alle hosts? Hvilke findes/findes ikke?

    Hvilket netværk er I på? Måske skal I have en host på `10.56.0.0/16`?

### Links

* [nmap intro](https://nmap.org/book/man.html)


## Exercise 2 - Services og os version

Med scripted fra ex 1 burde denne opgave være "nem".

### Opgave beskrivelse

1. Scan top 5 porte på et udsnit af hosts fra listen

    Koordinér hvor der scanner hvam, så vi ikke generer den samme host for meget.

2. Konklusioner? Hvordan bruger man data? `-oX` eller `-oG`?

### Links

Ingen

## Exercise 2b - Extra stuff

Nmap kan meget...

### Opgave beskrivelse

1. Find noget sejt nmap kan, og vis der til os andre.

    Hint: [nmap NSE](https://www.youtube.com/watch?v=M-Uq7YSfZ4I)

### Links

Ingen


## Exercise 3 - OpenSCAP forsøg

Brug openscap til at checker Jeres server.

### Opgave beskrivelse

1. Afprøv OpenSCAP.

    [En guide](https://www.open-scap.org/getting-started/)


Der er en joker med at openscad er et red hat project, så det vil være nemmest at afprøve det på en red hat eller centos linux boks.

### Links

Ingen


## Exercise 4 - OpenVAS

Brug openscap til at checker Jeres server.

### Opgave beskrivelse

1. Installer openvas på kali.

    [En tutorial](https://hackertarget.com/install-openvas-gvm-on-kali/), [endnu en](https://linuxhandbook.com/openvas-tutorial/)

2. Test den mod servere

    Start med den server som i brugte til at arbejde med metasploit.

### Links

Ingen
