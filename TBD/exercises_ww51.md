---
Week: 51
tags:
- Red teaming
- CSC
---

\pagebreak

# Opgaver for ww51

## Exercise 1 - CSC 20

CSC20 er om pen testing og red teaming

### Opgave beskrivelse

1. Hent den seneste CIS CSC 20 fra deres hjemmeside.

2. Læs op på nummer 20, og de tilhørende subcontrols.

3. Lav en opsummering - PP og diskusison på klassen

  Hvem er det relevant for? Hvad er forskellen på pen testing og red teaming? Hvad skal undersøges?

### Links

* Ingen



## Exercise 2 - Skole red teaming

Vi har et red team projekt kørende. Antag at objektivet er at få adgang til karakterene for en specifik klasse.


### Opgave beskrivelse

1. Lav en "attack graph" over hvordan man kan få adgang til dataene.

1. Find en vej der ikke inkluderer at snyde personer.

3. Find en vej der er ren netværksteknisk.


### Links

* Ingen
