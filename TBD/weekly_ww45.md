---
Week: 45
Content:  Lag 1 og 2
Material: See links in weekly plan
Initials: MON
---

# Uge 45

Ugens emne er sikkerhed på lag 1 og 2.


## Mål for ugen

Praktiske og læringsmål for ugen er

### Praktiske mål

Ingen

### Læringsmål

Den studerende kan

* forklare koncepterne og opsætte et trådløst netværk
* forklare og bruge network access control (specifikt 802.1x)

## Leverancer

Ingen


## Tidsplan

Nedenfor er den foreløbige tidsplan, som vil ændre sig afhængigt af input fra de studerende (m.fl.)

### Fredag

Tidsplanen:

| Tidspunkt | Aktivitet |
| :---: | :--- |
| 8:15 | MON starter op |

MON har omeksamen fra 9 til 11, og vil være tilgængelig på riot.

Et par studerende fra ITT3 vil nok komme og arbejde på NAC bypass.

Links:

* NAC intro from [juniper](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=3&ved=2ahUKEwjRwNzIrdHlAhUMjqQKHYcCBw4QFjACegQIABAC&url=https%3A%2F%2Fwww.juniper.net%2Fdocumentation%2Fen_US%2Flearn-about%2FLA_802.1X_NAC.pdf&usg=AOvVaw3RDQt2RfFXYMdSmvpVyCk5)
* NAC intro from ["mikeguy"](https://mikeguy.co.uk/posts/2018/06/understanding-nac-802.1x-and-mab/)


## Hands-on tid

Opgaver er på [gitlab](https://ucl-pba-its.gitlab.io/22s-its-nwsec/22S_ITS_nwsec_exercises.pdf)

## Kommentarer

Ingen pt.
