---
Week: 48
Content:  Topologies (again)
Material: See links in weekly plan
Initials: MON
---

# Uge 48

Vi har snakket om topologier før - og nu gør vi det igen.

Keywords: segmentation (e.g. DMZ, non-routable), lateral movement

## Mål for ugen

Praktiske og læringsmål for ugen er som følger

### Praktiske mål

* Ingen

### Læringsmål

Den studerende kan

* Forklare netværkssegmentering, både fordele og ulemper
* Bruge netværkssegmentering til at sikre netværksdesign

## Leverancer

Ingen

## Tidsplan

Nedenfor er den foreløbige tidsplan, som vil ændre sig afhængigt af input fra de studerende (m.fl.)

### Fredag

Tidsplanen:

| Tidspunkt | Aktivitet |
| :---: | :--- |
| 8:15  | MON starter op |
| 10:30 | opgave 1 gennemgang |

MON har møder om eftermiddagen.

Links:

* [lateral movement fra trend micro](https://blog.trendmicro.com/cyberattack-lateral-movement-explained/)
* [Common techniques](https://resources.infosecinstitute.com/category/certifications-training/ethical-hacking/post-exploitation-techniques/lateral-movement-techniques/)
* [Crowdstrike's take on it](https://www.crowdstrike.com/epp-101/lateral-movement/) - Reklameagtig, men har nogle gode tanker.
* [MITRE tactics](https://attack.mitre.org/tactics/TA0008/)
* [CIS 20 CSC #14](https://www.cisecurity.org/controls/controlled-access-based-on-the-need-to-know/)
* [Port isolation on cable](https://en.m.wikipedia.org/wiki/Private_VLAN)
* [video om flat vs. segmented networks](https://www.youtube.com/watch?v=Xjo9UG90EOo)
* [tilhørende slide](https://ucl-pba-its.gitlab.io/22s-its-nwsec/2019-10-28-Note-21-09-flat-segmented.pdf)

## Hands-on tid

Opgaver er på [gitlab](https://ucl-pba-its.gitlab.io/22s-its-nwsec/22S_ITS_nwsec_exercises.pdf)

## Kommentarer

Ingen pt.
