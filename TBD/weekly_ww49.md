---
Week: 49
Content:  Passive monitoring
Material: See links in weekly plan
Initials: MON
---

# Uge 49

Passive monitoring er når man opsamler data uden selv at gøre noget aktivt, e.g. læse log filer, DNS, sniffe trafik

## Mål for ugen

Praktiske og læringsmål for ugen er som følger

### Praktiske mål

* Ingen

### Læringsmål

Den studerende kan

* Forklare forskellen på aktiv og passive monitorering(intelligence)
* Udføre passiv monitorering med passende værktøjer

## Leverancer

Ingen

## Tidsplan

Nedenfor er den foreløbige tidsplan, som vil ændre sig afhængigt af input fra de studerende (m.fl.)

### Fredag

Tidsplanen:

| Tidspunkt | Aktivitet |
| :---: | :--- |
| 8:15  | MON starter op |


Links:
* Der kommer måske en passive vs. active video
* OSINT (open source intelligence), e.g [dnsdumpster](https://dnsdumpster.com/). Also check the [learn](https://dnsdumpster.com/footprinting-reconnaissance/) and [defend](https://dnsdumpster.com/osint-network-defenders/)
* OSINT tools [list from gray campus](https://www.greycampus.com/blog/information-security/top-open-source-intelligence-tools)



## Hands-on tid

Opgaver er på [gitlab](https://ucl-pba-its.gitlab.io/22s-its-nwsec/22S_ITS_nwsec_exercises.pdf)

## Kommentarer

* Vi ser ofte på det tekniske, men mange as OSINT værktøjerne giver ikke-tekniske oplysnigner om personer og organisationer.
