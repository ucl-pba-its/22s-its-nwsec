---
title: '22S PBa IT sikkerhed'
subtitle: 'Eksamensprocedure for Netværks- og kommunikationssikkerhed'
main_author: 'Morten Bo Nielsen/MBNI'
date: \today
email: 'mbni@ucl.dk'
left-header: \today
right-header: 'Eksamensprocedure nwsec'
skip-toc: true
skip-lof: true
#status: foreløbig
---

# Baggrund

Tidligere dokumenter

* [Fagplanen](https://ucl-pba-its.gitlab.io/19-its-docs/22S-ITS-netværks--og-kommunikationssikkerhed.pdf)
* [Ugeplaner](https://ucl-pba-its.gitlab.io/22s-its-nwsec/22S_ITS_nwsec_weekly_plans.pdf) (se også [hjemmesiden](https://ucl-pba-its.gitlab.io/22s-its-nwsec/))
* Studieordningen, [National del](https://www.ucl.dk/link/3a5f054541da4bbca44c677abf1ee8c3.aspx?nocache=96d4162e-02ec-43e7-bad6-62069d54eef5) og [institutionsdel](https://esdhweb.ucl.dk/D21-1703583.pdf?nocache=23d8b5d5-f55c-434e-b516-23723ad7c741)

# Eksamen

Onsdag d. 8. juni og torsdag d. 9. juni 2022 er afsat. Den specifikke tidsplan kommer senere.

Fra studieordningen

> Prøven er en individuel, mundtlig prøve med udgangspunkt i et spørgsmål, som den studerende trækker til eksamen.
> 
> Alle spørgsmål, der kan trækkes til eksamen, er udleveret til de studerende senest 14 dage før eksamen, så de studerende har mulighed for at forberede sig. Hvad angår spørgsmålene, vil der blive lagt vægt på, at den studerende kan inddrage eksempler fra projektarbejdet og praktiske øvelser fra det forgangne semester. Der er ingen forberedelse på selve dagen.
> 
> Den individuelle, mundtlige eksamen varer 25 minutter inkl. votering
> 

Karakterer efter 7-trins skalaen og der er ekstern censur.

# Undervisers forventning til eksamenen

Til eksamenen skal den studerende vise at læringsmålene er opfyldt. Der er som bekendt langt mellem 02 og 12, og studerende kan vælge en strategi der passer til den enkeltes temperament, evner og stil.

Generelt set, så ønsker vi at se og høre, at den studerende har en god forståelse for hvordan forskellige typer data flyder rundt i et netværk og kan relatere det til sikkerhed. Det foreslås at der laves et simpelt netværkseksempel eller scanarie, som dækker hvad I ønsker at sige. 

Der trækkes et emne, og det forventes, at den studerende præsentere i ca. 10 minutter, hvorefter der vil være spørgsmål. Eksaminator og censor vil søge at afdække den studerendes niveau i hele pensum med udgangspunkt i det trukne emne.

Eftersom kommunikation er en vigtig del af uddannelsen forventes det også at præsentationer og demonstrationer er gearet til publikum, ie. underviser og censor, som må forventes at have et højt niveau inden for netværk og sikkerhed generelt.


# Emneliste

Nedenstående er listen over emner som kan trækkes. For alle spørgsmål er "i et sikkerhedsperspektiv" underforstået. 

* Krypteret trafik: Formål, muligheder/begrænsninger, performance, aflytte plaintext/ciphertext/metadata
* Segmentering: Routable og non-routable subnets, tunneller, firewalls, L2 security, performance/DOS, lateral movement
* Monitorering (funktionel): Beskriv de 3 typer (rød/grøn, grafer og logs) og hvad de bruges til både reaktivt og proaktivt
* Load balancers og reverse web proxies: Beskrivelse og funktion i et netværk, certifikater, performance, redundans, multiple virtual hosts
* Proxy: Definition, protokoller, problemer og muligheder, kryptering, aflytte trafik
* DNS: Resolve hostnavne, kryptering, certifikater, DNS og privacy
* Firewalls: Funktion i netværket, statefull/stateless, block/accept/reject, perimeter sikkerhed pro/con
* SSL inspection og next-gen firewall: Implementation, certifikater, placering i netværket, problemer/muligheder for netværks ejeren og brugere

