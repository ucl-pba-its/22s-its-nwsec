---
title: '22S PBa IT sikkerhed'
subtitle: 'Fagplan for Netværks- og kommunikationssikkerhed'
main_author: 'Morten Bo Nielsen'
date: \today
email: 'mbn@mnworks.dk'
left-header: \today
right-header: 'Fagplan for Netværks- og kommunikationssikkerhed'
skip-toc: true
skip-lof: true
filename: 22S-ITS-NWSEC_lecture_plan
semester: 22S
---

# Oversigt over ugplanerne

Nedenstående er taget fra ugeplanerne.

De skal sammenlignes med læringsmålene. Se e.g. [fagplanen](/22s-its-nwsec/22S_ITS1_NWSEC_lecture_plan.pdf)


## Week 06

### Praktiske mål

* Ingen

### Læringsmål

* Den studerende ved hvad faget går ud på
* Den studerende kender til OSI modellen og struktureret fejlfinding
* MON kender niveauet

## Week 07

### Praktiske mål

None

### Læringsmål

* Den studerende kan forklare OSI modellen og de tilhørende lag
* Den studerende kan placere hardware på de forskellige lag i OSI modellen.
* Den studerende kan benchmarke båndbredde
* Den studerende kan forklare konceptet "bottlenecks" og dets relevans i IT sikkerhed

## Week 08

### Praktisk mål

* Den studerende har vmware workstation kørende, og har en Kali live

### Læringsmål

* Den studerende kan forklare fordele og ulemper ved forskellige topologier
* Den studerende kan forklare segmentering, og dets relation til sikkerhed og netværksperformance
* Den studerende kender til hvordan netværksenheder bruges

## Week 09

### Praktiske mål

* Den studerende har en minimal Debian headless server og en openbsd router.

### Læringsmål

* Den studerende kan designe et netværk ud fra simple specifikationer
* Den studerende kan designe et IP layout for et simpelt netværk
* Den studerende kan forklare forskellene på fysiske og virtuelle enheder (interfaces, servere, LANs)
* Den studerende kan forklare hvad netflow er og hvad det kan bruges til

## Week 10

### Praktiske mål

Ingen.

### Læringsmål

* Den studerende kan forklare TLS
* Den studerende kan bruge relevante analyse værktøjer til at undersøge netværkstrafikken.


## Week 11

### Praktiske mål

* Den studerende har en monitoreringsserver kørende med grafana, prometheus mv.

### Læringsmål

* Den studerende kan forklare de tre forskellige typer af monitorering
* Den studerende kan forklare hvilke data der er tilgængelige fra hvilke typer enheder
* Den studerende kan udføre simpel fejlfinding ud fra monitorering


## Week 12

### Praktiske mål

Ingen

### Læringsmål

* Den studerende kan forklare DNS og issues i forbindelse med CIA
* Den studerende kan forklare design elementer i almindelig web server setups
* Den studerende kan udføre passive dns opslag og forklare relevansen.


## Week 13

### Praktiske mål

Ingen

### Læringsmål

* Den studerende kan forklare sikkerhed på lag 1 og 2, vha. CIA.
* Den studerende kan beskrive almindelige angreb på lag 1 og 2.


## Week 14

### Praktiske mål

Ingen

### Læringsmål

* Den studerende kan forklare sikkerhed på lag 3 og 4, vha. CIA.
* Den studerende kan beskrive almindelige angreb på lag 3 og 4.


## Week 15

### Praktiske mål

Ingen

### Læringsmål

* Den studerende kan forklare formålet med dynamisk opdaterede firewalls, eg. vha. crowdsec
* Den studerende kan starte virtuelle maskiner i skyen
