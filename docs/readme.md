---
Title: does it work
Subtitle: i hope it does
Main_author: NISI
---

Documents
==================

Der er her vi lægger formelle dokumenter, som er/bliver til PDF dokumenter til undervisere og studerende.

Alle .md file vil blive konverteret til pdf og lagt på gitlab pages. Denne readme er undtaget.

Front matter
----------------

Front matter indeholder metadata om indholdet. Disse felter bliver brugt forskellige steder i genereringen af pdf og html.

se [document template eksemeplet]([https://moozer.gitlab.io/document_templates/test.md) for fronte matter muligheder.

Pga. html genereringen, så virker `date: \today` ikke. Bruger istedet `date: 2020-02-02` eller undlad den for at bruge indeværende dato.
