#!/usr/bin/env python3

# inspiration from
# https://gist.github.com/jiffyclub/5015986

import argparse
import sys

import jinja2
import markdown
from bs4 import BeautifulSoup
from datetime import date, timedelta


def parse_args(args=None):
    d = 'Make a complete, styled HTML document from a Markdown file.'
    parser = argparse.ArgumentParser(description=d)
    parser.add_argument( '-i', '--input', type=argparse.FileType('r'),
                        default=sys.stdin,
                        help='Markdown file to convert. Defaults to stdin.')

    parser.add_argument('-o', '--out', type=argparse.FileType('w'),
                        default=sys.stdout,
                        help='Output file name. Defaults to stdout.')

    parser.add_argument( '-p', '--pages_url', required=True,
                        help='Base URL for pages. Weekly subdir and such will be appended.')

    return parser.parse_args(args)

def extract_ww_table( html ):

    soup = BeautifulSoup(html, 'html.parser')
    ww_table = soup.find( 'table')
    ww_table_body = ww_table.find( 'tbody')

    ww_data = []
    ww_rows = ww_table_body.find_all( 'tr')
    for ww in ww_rows:
        cols = ww.find_all('td')
        cols = [ele.text.strip() for ele in cols]
        ww_data.append(cols)

    return ww_data

def monday_of_calenderweek(year, week):
    first = date(year, 1, 1)
    base = 1 if first.isocalendar()[1] == 1 else 8
    return first + timedelta(days=base - first.isocalendar()[2] + 7 * (week - 1))

def convert_ww_to_dict( ww_data, year ):
    dict = []
    for ww in ww_data:
        dstart = monday_of_calenderweek( year, int(ww[1]) )
        dend = dstart+timedelta(days=4)
        dict.append( { 'week': int(ww[1]), 'teacher': ww[0], 'description': ww[2],
                       'date_start': dstart, 'date_end': dend } )

    return dict

def main(args=None):
    args = parse_args(args)
    md_text = args.input.read()

    extensions = ['extra', 'meta']
    md = markdown.Markdown(extensions=extensions)
    html = md.convert( md_text )

    ww_data = extract_ww_table(html)
    ww_dict = convert_ww_to_dict(ww_data, 2000+int(md.Meta['semester'][0][:2]))

    template = open('itslearning.xml.j2', 'r').read()
    doc = jinja2.Template(template).render( course_name=md.Meta['title'][0].strip("'"),
                                            course_plan_filename=md.Meta['filename'][0].strip("'\"")+'.pdf',
                                            pages_url=args.pages_url,
                                            ww_dict=ww_dict)
    args.out.write(doc)

if __name__ == '__main__':
    sys.exit(main())
