#!/usr/bin/env bash

PDFDIR=$1

cat << EndOfMessage
---
layout: main
title: PDFs
permalink: /pdf
---

<section class="pdf-overview">
<h1>Available PDFs</h1>
<ul>
EndOfMessage

for F in $PDFDIR/*.pdf; do
  echo "  <li><a href=\"{{ baseurl }}$F\">$(basename $F)</a></li>"
done

cat << EndOfMessage
</ul>
</section>
EndOfMessage
