8.15 hello

who am i
hvad er dette fag
* studieordningen
* fagplanen
* noget med eksamen
* metode mv.
    * I arbejder selv med det som ikke kræver min tilstedeværelse.
        * gruppearbejde gør I selv.
    * Vi laver ting på klassen, som kræver hjælp eller er godt til plenum.
    * Spørg i chatten - måske svarer jeg, måske svarer en kalsse kammerat.
        * husk at tagge mig

9:00
* pause? 10 min

9:10

who are you
* diplom
* ITT
* DMU
* andet

Hvad kan I?

Matrix på tavlen:

* kender til det/har arbejdet med det/kan det bare
* SOHO networking
* linux
* windows
* python
* .NET
* bash
* IT in business
* defining Processes/procedures
* virtuelle maskiner

Alle mand til tavlen. Sæt en streg.

9:55
* pause 10 min?

## 10:05 OSI modellen

* Hvad er OSI modellen?
* Hvad kan den bruges til?
* Hvad ser en programmør, hvad ser en netværksmand?
    * 5 eller 7 lag?
* Var der nogle Netværks hajer i klassen?

### Stuktureret fejlfinding
* se opgave...

En bruger siger "det virker ikke" - og hvad så?
* det er et startsted.

Alle computere er kommunikationssystemer, og de arbejder med data.
* Data kommer et sted fra
* Data bliver transportet, bearbejdet og gemt
* Det er meget nemmere at se hvad der bliver sendt mellem blokken, end at åbne blokkene.

Opbyg en blokdiagram-forståelse af det system du arbejder med.

2 modeller:
* YOLO
* stuktureret

#### YOLO/spredhagl modellen

* det er nok dette her der er galt
    * teste, rette, teste igen
    * virker/virker ikke
    * forfra
* Det virker meget tit og hurtigt (pga. erfaring)
* masser af lappeløsning
    * recurrent fejl
* man stopper når der virker (men kommer det igen?!)
* Hvor tit hører I "hmmm, det har vi da set før, og hvad det lige vi gjorde?" 
    * og hvis der ikke er noter eller andet, så er I nok i YOLO kategorien

#### Root cause/proper incidence handling
1. Start ved der hvor man ser fejlen (f.eks. bruger ser noget)
    * Notér hvad der ses, hvad fejlen er og den slags.
2. Opstille hypotese om fejlen
    * Best guess, så man har en retning til at søge informationer
3. Indsamle info
    * monitorering
    * spørge bruger
    * log filer
    * Køre tests
        * Kan fejlen reproduceres? hvis ja, YEAH!, så kan kan vi også checke om den er rettet.
    * andet
4. Kan hypotese be- eller afbekræftes?
    * hvis afbekræftes, Back to 2. og inkludér de nye oplysninger
    * hvis bekræftet, så har man root cause
5. Beslut hvad der skal gøres.
    * at lappe kan være en løsning.
    * at definere en procedure
    * at updatere/justere systemet
    * det er et resspurce spørgsmål.

Hvis man vi gøre begge dele samtidigt, så skal man være god til at koordinere.
* en der er stuktureret, og en der YOLO'er.
* fejlen kan findes hurtigere, hvis man er heldig/god

In conclusion.
* TAG NOTER!!

## 11:15 Lektier

Opgave 1+2, og vi skal bruge vmware, så den skal I også snart have kørende.

## 11:30 slut
