---
Week: 06
Content:  Fagopstart
Material: See links in weekly plan
Initials: MON
---

# Uge 6

Dette er første session. Så vi kommer til at bruge en del tid på at lære hinanden og faget at kende.

## Mål for ugen

Practical and learning goals for the period is as follows

### Praktiske mål

* Ingen

### Læringsmål

* Den studerende ved hvad faget går ud på
* Den studerende kender til OSI modellen og strujtureret fejlfinding
* MON kender niveauet

## Leverancer

* Diskussion på klassen

## Tidsplan

Nedenfor er den foreløbige tidsplan, som vil ændre sig afhængigt af input fra de studerende (m.fl.)


### Mandag

Tidsplanen:

| Time | Activity |
| :---: | :--- |
| 8:15 | MON introducerer faget |
| 9:00 | Vi snakker fagligt niveau |
| 10:00 | MON introducerer OSI modellen |
| 10:40 | MON introducerer fejlfinding |
| 11:15 | Gennemgang af lektier til næste gang |
| 11:30 | Vi slutter |


## Kommentarer
* Links om OSI modellen se [her](https://www.comparitech.com/net-admin/osi-model-explained/) og video [her](https://www.youtube.com/watch?v=vv4y_uOneC0)

## struktureret fejlfinding

* Root cause analysis
    * Fejlen er tydelig et sted, og så skal man følge kæden af data flow og processer tilbage til hvor den virkelige fejl er.
    * og dokumenterer undervejs
* YOLO
    * spreghagl, og når det virker igen så stopper man
    * uden at man kan reproducere eller ved hvad der var galt


## Lektier til næste gang

Opgave 1+2

