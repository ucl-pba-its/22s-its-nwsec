---
Week: 07
Content:  OSI og hardware
Material: See links in weekly plan
Initials: MON
---

\newpage

# Week 7

Vi skal snakke om hardware i dag.


## Goals of the week(s)
Practical and learning goals for the period is as follows

### Praktiske mål

None

### Læringsmål

* Den studerende kan forklare OSI modellen og de tilhørende lag
* Den studerende kan placere hardware på de forskellige lag i OSI modellen.
* Den studerende kan benchmarke båndbredde
* Den studerende kan forklare konceptet "bottlenecks" og dets relevans i IT sikkerhed

## Deliverables

None

## Schedule

Below is the tentative schedule, which may be changed depending on input from the students.

### Mandag

Tidsplanen:

| Time | Activity |
| :---: | :--- |
| 8:15  | Dagens plan |
| 8:30  | Lektier: OSI. 15 min i grupper, 15 min på klassen |
| 9:00  | Lektier: Fejlfinding. 15 min i grupper, 15 min på klassen |
| 9:45  | MON gennemgår CIA og flaskehalse |
| 10:30 | Speed tests og iperf |
| 11:15 | Lektier til næste gang |
| 11:30 | Vi slutter |

Gennemgang af lektier:

* Der laves mindre grupper, hvor I sammenligner det som I har lavet
* Vi tager de bedste eksempler på klassen.


## Kommentarer

### CIA links 

* [deepwatch](https://www.deepwatch.com/blog/cia-in-cybersecurity/)
* [forcepoint](https://www.forcepoint.com/cyber-edu/cia-triad)
* [comptia security+ youtube](https://www.youtube.com/watch?v=y3goanmMj3s)

Og det skal sammen holdes med data at rest, in transit og in process.

### Bottlenecks

* Start altid ved

  * Netværk
  * CPU
  * RAM
  * Disk I/O

* Der er altid en flaskehals et sted, ellers ville alting gå uendeligt hurtigt
* Kan man udnytte en flaskehals, er der potentiale for et denial-of-service angreb

    * DDOS fra Cloudflare - https://www.cloudflare.com/en-gb/learning/ddos/what-is-a-ddos-attack/ (Vendor site!)

* vi vender tilbage til dette når vi snakker monitorering
* Tænk i at data flyder mellem processeringsenheder (som vi snakkede om i sidste uge), og at der på vejen er begrænsninger.



## Lektier til næste gang

Opgave 1+2+3