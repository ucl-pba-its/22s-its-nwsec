---
Week: 09
Content:  Bygge netværk
Material: See links in weekly plan
Initials: MON
---

# Uge 9

Sidste uge snakkede vi om netværks topologi og hvordan man strukturere sit netværk for at få redundans og (lidt) sikkerhed.

Denne uge ser vi på hvordan man rent praktisk indretter sig med IP adresser, routere og VLANs.


## Mål for ugen

Pratical and learning goals for the period is as follows

### Praktiske mål

* Den studerende har en minimal Debian headless server og en openbsd router.

### Læringsmål

* Den studerende kan designe et netværk ud fra simple specifikationer
* Den studerende kan designe et IP layout for et simpelt netværk
* Den studerende kan forklare forskellene på fysiske og virtuelle enheder (interfaces, servere, LANs)
* Den studerende kan forklare hvad netflow er og hvad det kan bruges til


## Leverancer

Ingen


## Tidsplan

Nedenfor er den foreløbige tidsplan, som vil ændre sig afhængigt af input fra de studerende (m.fl.)

### Mandag

Tidsplanen:

| Time | Activity |
| :---: | :--- |
| 8:15 | Lektier: vmware, router, kali og virtuelle maskiner generelt |
| 9:15 | Ip layouts og netbox |
| 10:15 | netflow og ntop |
| 11:15 | lektier til næste gang |
| 11:30 | Frokost |


## Kommentarer

* Online companion til dagen er [her](https://moozer.gitlab.io/course-networking-security/03_network_layer/)


## Lektier til næste gang

* IPAM

    a. papir+blyant øvelse. 
        
        Lav en IP subnet oversigt over dine device og de tilhørende interne og eksterne netværk

    b. Set netbox op og dokumentér dit netværk

        Jeg hjælper gerne. 
        
        Gå evt. i gruppe med en diplomer for de har nok eet halvstort netværk...

    c. Se på det IPAM dokumentation som firmaet allerede har, og forhold dig til det.


* [Set up netflow](https://moozer.gitlab.io/course-networking-security/03_network_layer/netflow/#netflow-using-ntopng)
