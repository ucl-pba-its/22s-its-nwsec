---
Week: 10
Content:  Sniffe netværk og protokoller
Material: See links in weekly plan
Initials: MON
---

# Uge 10

Dagen emne er netværkstrafik og hvordan man analyserer det. Data skal samles op og vi ser på lidt tools til at arbejde med det.

Det forventes at den studerende har en grundlæggende forståelse for forskellige netværksprotokoller på forhånd, såsom DNS og HTTP.

## Mål for ugen

Pratical and learning goals for the period is as follows

### Praktiske mål

Ingen.

### Læringsmål

* Den studerende kan forklare TLS
* Den studerende kan bruge relevante analyse værktøjer til at undersøge netværkstrafikken.

## Leverancer

Ingen


## Tidsplan

Nedenfor er den foreløbige tidsplan, som vil ændre sig afhængigt af input fra de studerende (m.fl.)

### Mandag

Tidsplanen:

| Tidspunkt | Aktivitet |
| :---: | :--- |
|  8:15 | Lektier: IPAM |
|  8:45 | Lektier: netflow |
|  9:15 | pause :-) |
|  9:30 | om at sniffe trafik |
| 10:00 | TLS, kryptering og den slags | 
| 10.30 | pause |
| 10:45 | tcpdump og wireshark - på klassen |
| 11:30 | Frokost |
| 12:15 | netflow, wireshark and pcaps |
| 13:30 | lektier til næste gang |
| 13:45 | the end |


## Kommentarer

* Online companion til dagen er [her](https://moozer.gitlab.io/course-networking-security/04_packets/)
* [wirehark intro](https://www.varonis.com/blog/how-to-use-wireshark)
* [wireshark test pakker](https://wiki.wireshark.org/SampleCaptures)
* [tcpdump BPF examples](https://hackertarget.com/tcpdump-examples/)

## Lektier til næste gang

1. Protokoller
    1. Find to-tre protokoller fra wiresharks eksempler som du genkende 
    2. Forstå hvad der foregår, evt. vha. google
    3. Tag noter, og vis det næste gang

2. Lokal net/firma net
    1. Start wireshark og saml noget data op
    2. Gem PCAPS
    3. Se på det: er der ukendte protokoller? ukendte ip adresser? andet interessant?
    4. Dyk ned i 2-3 streams, og forstå hvad der foregår.
    5. Tag noter, og vis det næste gang

4. Lav SSL dump opgaven [herfra](https://moozer.gitlab.io/course-networking-security/04_packets/tls_protocol/)
