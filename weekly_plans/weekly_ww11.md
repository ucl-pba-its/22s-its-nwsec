---
Week: 11
Content:  Monitorering
Material: See links in weekly plan
Initials: MON
---

# Uge 11

Monitorering er en hjørnesten i al sikkerhed. Vi ønsker at have så meget synlighed i vores netværk som muligt, for at opdage anomalier - og for at fikse ting i driften som er forkerte.

Der er meget overlap mellem drift og sikkerhed, se f.eks. denne [artikel](https://medium.com/anton-on-security/stealing-more-sre-ideas-for-your-soc-e2fe6836fe9a).


## Mål for ugen

Pratical and learning goals for the period is as follows

### Praktiske mål

* Den studerende har en monitoreringsserver kørende med grafana, prometheus mv.

### Læringsmål

* Den studerende kan forklare de tre forskellige typer af monitorering
* Den studerende kan forklare hvilke data der er tilgængelige fra hvilke typer enheder
* Den studerende kan udføre simpel fejlfinding ud fra monitorering

## Leverancer

Ingen

## Tidsplan

Nedenfor er den foreløbige tidsplan, som vil ændre sig afhængigt af input fra de studerende (m.fl.)

### Mandag

Tidsplanen:

| Tidspunkt | Aktivitet |
| :---: | :--- |
| 8:15 | Lektier: wireshark protokoller |
| 8:45 | Lektier: wireshark sniffing og analyse |
| 9:15 | Lektier: ssldump |
| 9:45 | a quick primer on monitoring |
| 10:15 | Pause så kan kan checke teknikken |
| 10:30 | Ekstern gæst (online) om hvordan grafana & friends bliver brugt  |
| 11:45 | Frokost/MON er til møde |
| 12:35 | More monitoring |
| 13:30 | lektier til næste gang |
| 13:45 | the end |

## Kommentarer

* Online companion til dagen er [her](https://moozer.gitlab.io/course-networking-security/05_monitoring/)

## Lektier til næste gang

* lav opgaven [her](https://moozer.gitlab.io/course-networking-security/05_monitoring/software/)