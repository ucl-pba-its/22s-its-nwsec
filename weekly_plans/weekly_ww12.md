---
Week: 12
Content:  DNS and web 
Material: See links in weekly plan
Initials: MON
---

# Uge 12

Der er meget at snakke om når vi snakker web og DNS. Privacy, confidentiality og integrity blandt andet.


## Mål for ugen

Practical and learning goals for the period is as follows

### Praktiske mål

Ingen

### Læringsmål

* Den studerende kan forklare DNS og issues i forbindelse med CIA
* Den studerende kan forklare design elementer i almindelig web server setups
* Den studerende kan udføre passive dns opslag og forklare relevansen.

## Leverancer

Ingen

## Tidsplan

Nedenfor er den foreløbige tidsplan, som vil ændre sig afhængigt af input fra de studerende (m.fl.)

### Mandag

Tidsplanen:

| Tidspunkt | Aktivitet |
| :---: | :--- |
| 8:15 | Lektier: grafana, prometheus og loki |
| 9:30 | DNS gennemgang |
| 10:30 | Passive DNS highlight |
| 11:30 | Frokost/MON er til møde |
| 12:15 | APIs, WAF, proxy, http/s |
| 13:30 | lektier til næste gang |
| 13:45 | the end |

## Kommentarer

* DNS oversigt. se [her](https://moozer.gitlab.io/course-networking-basisc/06_domain_name_system/)
* DNS er meget problematisk på forskellige måder
    * see f.eks. [her](https://blog.uncensoreddns.org/blog/39-the-unfriendly-internet-turning-off-cleartext-lookups-in-september/) fra uncensoreddns.org.
    * Skal it afdelingen bestemme over DNS? [paul vixie](https://www.youtube.com/watch?v=ZxTdEEuyxHU). Spoiler: han er sur.
    * Mere DNS kryptering (https://www.youtube.com/watch?v=pjin3nv8jAo)
        * DNS over TLS, DNS over HTTPS
    * [encrypted dns fra cloudflare](https://blog.cloudflare.com/dns-encryption-explained/). Note: vendor link
    * Er centralisering af dns servere en god ting? 8.8.8.8, 1.1.1.1 og de andre store
    * data in transit, confidentiality, integrity?
* Passive DNS. [Farsight](https://www.farsightsecurity.com/) er dem jeg kender
    * Vi ville have brugt dnsscout som eksempel, men [booo](https://www.farsightsecurity.com/dnsdb-community-edition/)
    * En [video](https://info.farsightsecurity.com/hubfs/DNSDB%2020%20Flexible%20Search.mp4) om det
    * Måske er [security trails(https://securitytrails.com/corp/api)] relevant
    Hvor kommer data fra, og hvad kan det bruges til?
* API
    * hvis du selv laver dem så brug [swagger](https://swagger.io/) (official [example site](https://petstore.swagger.io/))
    * Mandatory [OWASP link](https://owasp.org/www-project-top-ten/) - som I nok har set før?
    * Design
        * Web application firewall - WAF: 
            * [cloudflare](https://www.cloudflare.com/en-gb/learning/ddos/glossary/web-application-firewall-waf/) (vendor link)
            *  nginx har [ACL'ere](https://docs.nginx.com/nginx/admin-guide/security-controls/controlling-access-proxied-tcp/) og en paid WAF. 
            * Et [eksempel på modsecurity](https://www.netnea.com/cms/apache-tutorial-7_including-modsecurity-core-rules/#step_4_triggering_alarms_for_testing_purposes). 
            * [Denne](https://coreruleset.org/documentation/) ser interessant ud. 
            * Og lidt [mere](https://www.feistyduck.com/library/modsecurity-handbook-free/online/index.html)
            * Disclaimer: Jeg er ikke hård til WAF'er.
        * [reverse proxies](https://www.cloudflare.com/en-gb/learning/cdn/glossary/reverse-proxy/) - vendor link
            * Load balancing, 
            * ssl offloading, ACME/let's encrypt
        * web cache, e.g. [varnish](https://varnish-cache.org/)
    * hvor krypterer vi?
        * data in transit, integritcy confidentiality
* Disclaimer om cloudflare. 
    * De er store, og laver mange gode ting, inkl. teknisk dokumentation. 
    * De er også et kommercielt firma som leverer netværksydelser, så et passende niveau af kildekritik er på sin plads. 
    * Deres dokumentation plejer at være meget sober sammenlignet med andre virksomheder.


## Lektier til næste gang

* opret en konto på security trail, free tier, og se hvad den siger om domæner du interesserer dig for
    * ucl.dk har f.eks. 255 underdomæner

