---
Week: 13
Content:  Recap security on layers 1-7 part 1
Material: See links in weekly plan
Initials: MON
---

# Uge 13

Vi er ca. halvvejs, så denne og næste uge vil være en form for recap.

Denne og næste uge tager vi trappen fra lag 1 til applikations laget, og snakker CIA.


## Mål for ugen

Practical and learning goals for the period is as follows

### Praktiske mål

Ingen

### Læringsmål

* Den studerende kan forklare sikkerhed på lag 1 og 2, vha. CIA.
* Den studerende kan beskrive almindelige angreb på lag 1 og 2.

## Leverancer

Ingen

## Tidsplan

Nedenfor er den foreløbige tidsplan, som vil ændre sig afhængigt af input fra de studerende (m.fl.)

### Mandag

Tidsplanen:

| Tidspunkt | Aktivitet |
| :---: | :--- |
| 8:15 | Lektier: securitytrail |
| 9:30 | Sikkerhed og problemer på lag 1-2 |
| 10:30 | MACsec og 802.1x |
| 11:30 | Frokost  |
| 12:15 | wireless, måske demo, måske kun snak |
| 13:15 | lektier til næste gang |
| 13:30 | the end |

## Kommentarer

* Sikkerhed på lag 1-2
* 802.1x 
    * "den sædvanelige" som man bruger til Network Access Control (NAC)
    * Man connecter til en port, logger ind, og bliver tildelt et vlan
    * Dette burde bare virke som klient med windows, mac, linux mv.
        * [linux guide](https://www.uio.no/english/services/it/network/student-residential-network/instructions/nm/)
    * Authentication modes from [juniper](https://www.juniper.net/documentation/us/en/software/junos/user-access/topics/topic-map/802-1x-authentication-switching-devices.html)
        * Obs: Der er flere modes og fall back til enheder som ikke kan 802.1x
    * Lidt fra [VMware](https://www.vmware.com/topics/glossary/content/network-access-control.html)
* IEEE 802.1AE (MACsec) 
    * Krypteret trafik mellem L2 enheder.
    * From [juniper](https://www.juniper.net/documentation/us/en/software/junos/security-services/topics/topic-map/understanding_media_access_control_security_qfx_ex.html)
    * from [zindagitech](https://zindagitech.com/what-is-ieee-802-1ae-macsec-how-does-it-work/)
    * Quick intro from [phil anderson](https://www.youtube.com/watch?v=u-nisjfHhtQ)
    * I am so not an expert in this
* Angreb
    * DHCP, 802.1x, VLAN, MAC spoof, mv
    * [ettercap](https://www.ettercap-project.org/) er en gammel kending. Der er også en [Fun with ettercap](https://pentestmag.com/article-fun-ettercap/)
    * Mht. Wireless ville jeg starte ved [aircrack-ng](https://www.aircrack-ng.org/). Og en [guide](https://nooblinux.com/crack-wpa-wpa2-wifi-passwords-using-aircrack-ng-kali-linux/), og en [demo](https://www.youtube.com/watch?v=uKZb3D-PHS0)

Det kan være forstyrrende, socialt uacceptabelt, brud på den lokale IT politik, og måske kriminelt at lege med denne slags. Slå hjernen til. 

## Lektier til næste gang

* Leg med ettercap
    * Genskab "Fun with ettercap" DNS delen
    * er det nemt? svært? 
    * hvordan beskytter man sig imod det?
* Leg med aircrack
    * Aflyst dit eget netværk derhjemme eller sæt et op til lejligheden.
    * er det nemt? svært? 
    * hvordan beskytter man sig imod det?

