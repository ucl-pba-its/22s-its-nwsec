---
Week: 14
Content:  Recap security on layers 1-7 part 2
Material: See links in weekly plan
Initials: MON
---

# Uge 14

Denne og sidste er en form for halvvejs recap, hvor vi går ISO modellen igennem og snakker CIA. 


## Mål for ugen

Practical and learning goals for the period is as follows

### Praktiske mål

Ingen

### Læringsmål

* Den studerende kan forklare sikkerhed på lag 3 og 4, vha. CIA.
* Den studerende kan beskrive almindelige angreb på lag 3 og 4.

## Leverancer

Ingen

## Tidsplan

Nedenfor er den foreløbige tidsplan, som vil ændre sig afhængigt af input fra de studerende (m.fl.)

### Mandag

Tidsplanen:

| Tidspunkt | Aktivitet |
| :---: | :--- |
| 8:15 | Lektier: ettercap og aircrack |
| 9:30 | Tunneller - sikkerhed og design tanker |
| 10:30 | BGP, TCP/UDP issues |
| 11:30 | Frokost  |
| 12:15 | Firewall regler - demo og diskussion |
| 13:15 | lektier til næste gang |
| 13:30 | the end |

## Kommentarer

* Sikkerhed på lag 3-5
* På lag 3 har vi forskellige tunneler
    * ikke krypterede e.g. gre, 4in6, 6in4. See [RFC2003](https://datatracker.ietf.org/doc/html/rfc2003) for the general idea.
    * krypterede, aka. VPN, såsom [wireguard](https://www.wireguard.com/), [openvpn](https://community.openvpn.net/openvpn/wiki/OverviewOfOpenvpn), PPTP, L2TP/IPSec, [SSTP] (https://docs.microsoft.com/en-us/openspecs/windows_protocols/ms-sstp/70adc1df-c4fe-4b02-8872-f1d8b9ad806a#:~:text=SSTP%20is%20a%20mechanism%20to,most%20firewalls%20and%20web%20proxies) (VPN over HTTPS - why not?)
* [BGP](https://www.juniper.net/documentation/us/en/software/junos/bgp/topics/topic-map/bgp-overview.html#:~:text=BGP%20is%20an%20exterior%20gateway,complete%20route%20to%20each%20destination.) issues
    * "Accidental reroute" from [US to china](https://arstechnica.com/information-technology/2018/11/strange-snafu-misroutes-domestic-us-internet-traffic-through-china-telecom/) and from [Europe to China](https://arstechnica.com/information-technology/2019/06/bgp-mishap-sends-european-mobile-traffic-through-china-telecom-for-2-hours/)
    * [Facebook outage in 2021](https://www.techtarget.com/searchnetworking/feature/3-lessons-from-the-2021-Facebook-outage-for-network-pros)
* [Connection hijacking](https://www.greycampus.com/opencampus/ethical-hacking/network-or-tcp-session-hijacking). It is a variation on MITM.
    * More from [tutorialspoint](https://www.tutorialspoint.com/ethical_hacking/ethical_hacking_tcp_ip_hijacking.htm)
* UDP amplification attacks
    * Since UDP is connectionless, this is a real problem, see e.g. [uncensoreddns blog](https://blog.uncensoreddns.org/blog/39-the-unfriendly-internet-turning-off-cleartext-lookups-in-september/) on why they stop unencrypted DNS.
    * From cloud flare, [DNS](https://www.cloudflare.com/en-gb/learning/ddos/dns-amplification-ddos-attack/) and [NTP](https://www.cloudflare.com/en-gb/learning/ddos/ntp-amplification-ddos-attack/)
* Firewall rules
    * [pf on openbsd](https://www.openbsd.org/faq/pf/example1.html)
    * [iptables on linux](https://www.cyberciti.biz/tips/linux-iptables-examples.html)
    * 
    * Some [Juniper](https://www.juniper.net/documentation/us/en/software/junos/flow-packet-processing/topics/topic-map/security-packet-based-forwarding.html#:~:text=An%20SRX%20device%20operate%20in,on%20a%20per%2Dpacket%20basis.) devices can do "selective stateless".

## Lektier til næste gang

* Det er påske. Lad os sige at vi reviewer hvad vi har lavet so far.
    * Kig ugerne igennem.
    * Formuler et spørgsmål per uge
    * Fremhæv en ting per uge som er cool, interessant, eller som på anden vis kan fremhæves.

