---
Week: 18
Content: Application layer security. NG firewalls. Certificates
Material: See links in weekly plan
Initials: MON
---

# Uge 18

Vi vil dykke mere ned i certifikater og hvordan man tænker når de skal bruges.

## Mål for ugen

Practical and learning goals for the period is as follows

### Praktiske mål

Ingen

### Læringsmål

* Den studerende kan forklare hvad PKI er og hvad det bruges til
* Den studerende kan oprette og bruge certifikater
* Den studerende kan forklare konceptet ssl interception og hvordan det implementeres

## Leverancer

Ingen

## Tidsplan

Nedenfor er den foreløbige tidsplan, som vil ændre sig afhængigt af input fra de studerende (m.fl.)

### Mandag

Tidsplanen:

| Tidspunkt | Aktivitet |
| :---: | :--- |
|  8:15 | Lektier: DO spinup, auth logs |
|  9:15 | Certificates 101 |
| 10:00 | MDAM om praktik |
| 10:30 | Easy-rsa |
| 11:15 | Frokost  |
| 12:00 | NGFW og mitmproxy |
| 13:15 | lektier til næste gang |
| 13:30 | the end |

## Kommentarer

* Links to come.
* Basic certifikat koncepter:
    * x509, private key, certificate, signing requests, revocation, self-signed, certificate chains, PKI
    * openssl tool
* [Easy-rsa](https://easy-rsa.readthedocs.io/en/latest/) demo
* [mitmproxy](https://mitmproxy.org/). Installation script is [here](https://gitlab.com/-/snippets/2309755)

## Lektier til næste gang

* Test easy-rsa
    1. Opret en easy-rsa pki
    2. Opret en ny vm med en webserver
    3. Opret et certifikat til webserveren
    4. tilføj certifikatet til webserveren
    5. check at det virker vha. browser og/eller curl