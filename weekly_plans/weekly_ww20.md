---
Week: 20
Content:  Active monitoring
Material: See links in weekly plan
Initials: MON
---

# Uge 20

Aktiv monitorering kender vi at scanne. Dette er ofte støjende og nemt at opdage. Det er også et godt værktøj for en adminsitrator, som ønsker at vide hvad der er på deres netværk.


## Mål for ugen

Praktiske og læringsmål for ugen er som følger

### Praktiske mål

(ingen)

### Læringsmål

* Den studerende kan aktiv monitorering og scanne med passende værktøjer
* Den studerende kan forklare attack surface, og interne vs. eksterne tools.

## Leverancer

* Ingen

## Tidsplan

Nedenfor er den foreløbige tidsplan, som vil ændre sig afhængigt af input fra de studerende (m.fl.)

### Mandag

Tidsplanen:

| Tidspunkt | Aktivitet |
| :---: | :--- |
|  8:15 | Lektier fra sidst: Mitra att&ck |
|  9:15 | Scanning, sniffing og attack surfaces |
| 10:15 | Intern scanning |
| 11:15 | Frokost  |
| 12:00 | Scannere på internettet: whois, ssllabs, shodanhq, security trail|
| 13:20 | lektier |
| 13:30 | the end |

## Kommentarer

* I kender allerede til passive og aktive handlinger, sniffing og så videre
    * [Mitre active scanning](https://attack.mitre.org/techniques/T1595/)
    * [CIS critical controls](https://www.cisecurity.org/controls/cis-controls-navigator/): e.g control 1: inventory, eller control 7: Continuous Vulnerability Management
* Attack surface
    * alle programmer, services og enheder som kan angribes, ie. det hele. Traditionelt set kun det der vender mod internettet, men det er ved at ændre sig.
        * vi vil have fokus på netværksdelen
    * fra [okta](https://www.okta.com/identity-101/what-is-an-attack-surface/)
    * fra [fortinet](https://www.fortinet.com/resources/cyberglossary/attack-surface)
    * kombiner attack surface og attack vectors med lateral movement
        * tænk intern og ekstern attack surface, og attack surface på alle enheder.
    * Hvordan afhænger dette af lokaliteten i netværket?

* Interne tools
    * [nmap](https://nmap.org/) er uundgåelig
        * [nmap intro](https://nmap.org/book/man.html)
        * [eksempler fra phoenixnap](https://phoenixnap.com/kb/nmap-command-linux-examples)
        * [flere eksempler fra cyberciti](https://www.cyberciti.biz/security/nmap-command-examples-tutorials/)
        * [en oldie but goodie](https://www.youtube.com/watch?v=M-Uq7YSfZ4I)
        * [nse examples fra redhat](https://www.redhat.com/sysadmin/nmap-scripting-engine)
    * [sslscan](https://www.kali.org/tools/sslscan/): en af mine personlige favoritter
        * den er 10 år gammel, men stadigt valid :-)
    * [openvas](https://www.openvas.org/)/nessus virker mere som "system sikkerhed" tools
        * download en VM hvis I vil [teste](https://www.greenbone.net/en/testnow/)
        * [architecture](https://greenbone.github.io/docs/latest/background.html#architecture)


* Internet baserede scannere
    * noget kører kontinuert, andre on demand
    * hvorfor køre det via en internet service og ikke direkte hjemmefra?
    * whois: [whois on ucl.dk](https://mxtoolbox.com/SuperTool.aspx?action=whois%3aucl.dk&run=toolpage)
    * [ssllabs](https://www.ssllabs.com/ssltest/): [test af ucl.dk](https://www.ssllabs.com/ssltest/analyze.html?d=ucl.dk&latest)
        * sslscan gør det samme.
    * security trails
        * De har en [ASI platform](https://securitytrails.com/corp/attack-surface-intelligence)
        * Vi har allerede snakket om denne
    * shodanhq


Links:
* [CIS CSC20 #3 Continuous Vulnerability Management](https://www.cisecurity.org/controls/continuous-vulnerability-management/)
* [Vulnerability management](https://www.dnsstuff.com/network-vulnerability-scanner) - (kildekritik: afsender ikke vetted)

OBS! (med udråbstegn): Vi laver aktive ting på netværket ofte mod servere og services der kører. Så slå hjernen til og lav ikke en `hail mary` ting, hvis det ikke virker i første forsøg. Vi har mulighed for at forstyrre netværket og services - hvis vi vil være destruktive, så skal det være med vilje.

Der er ting som jeg synes er relevante, men som ikke er spot on
* Reconnaisance på ipv6 netværk: [RFC 7707](https://tools.ietf.org/html/rfc7707) og [RFC 9099](https://datatracker.ietf.org/doc/html/rfc9099)
* Openscap: [wikipedia](https://en.wikipedia.org/wiki/Security_Content_Automation_Protocol), [getting started](https://www.open-scap.org/getting-started/), [howto](https://www.youtube.com/watch?v=6ehIeAxzXSY)


## Lektier til næste gang

1. Se emnelisten igennem
2. Sammenlign med Jeres noter
3. Skriv to-tre sætninger til hver om hvad I vil snakke om i 5'ish minutter
4. Vi snakker om det på mandag