---
Week: 21
Content:  Recap/eksamenssnak
Material: See links in weekly plan
Initials: MON
---

# Uge 21

Aktiv monitorering kender vi at scanne. Dette er ofte støjende og nemt at opdage. Det er også et godt værktøj for en adminsitrator, som ønsker at vide hvad der er på deres netværk.


## Mål for ugen

Praktiske og læringsmål for ugen er som følger

### Praktiske mål

* De studerende har en god forståelse for eksamensemne og proceduren

### Læringsmål

* ingen

## Leverancer

* Ingen

## Tidsplan

Nedenfor er den foreløbige tidsplan, som vil ændre sig afhængigt af input fra de studerende (m.fl.)

### Mandag

Tidsplanen:

| Tidspunkt | Aktivitet |
| :---: | :--- |
|  8:15 | Vi snakker eksamen |
|    ? | the end |

Vi stopper når vi er færdige.

## Kommentarer

ingen


## Lektier til næste gang

Det er op til Jer. Næste "gang" er eksamenen :-)